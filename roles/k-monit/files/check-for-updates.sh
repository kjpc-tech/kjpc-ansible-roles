#!/bin/sh

STATUS_OK=0
STATUS_UNKNOWN=1
STATUS_WARNING=2
STATUS_CRITICAL=3

# get updates
updates=$(/usr/lib/update-notifier/apt-check 2>&1)
if [ $? -ne 0 ]; then
    echo "unknown"
    exit $STATUS_UNKNOWN
fi

# no updates
if [ "$updates" = "0;0" ]; then
    echo "no updates"
    exit $STAUS_OK
fi

# security updates
pending=$(echo "${updates}" | cut -d ";" -f 2)
if [ "$pending" != "0" ]; then
    echo "${pending} security update(s)"
    exit $STATUS_CRITICAL
fi

# regular updates
pending=$(echo "${updates}" | cut -d ";" -f 1)
if [ "$pending" != "0" ]; then
    echo "${pending} regular update(s)"
    exit $STATUS_WARNING
fi

echo "Unknown error."
exit $STATUS_UNKNOWN
