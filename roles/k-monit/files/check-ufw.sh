#!/bin/sh

## Get the script output and return value
OUTPUT=$(systemctl is-active ufw)
RETURN=$?

## Output the command output
echo $OUTPUT

## Exit with return value of command
exit $RETURN
