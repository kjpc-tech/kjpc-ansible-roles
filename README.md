# kjpc-ansible-roles

## [Ansible](https://www.ansible.com/) roles for web servers.
Ansible roles that extend [cfd-common-roles](https://gitlab.com/cdetar/cfd-common-roles). Adds a new Django role, along with a Flask site role, a static site role, and a Monit role.

**Requires [cfd-common-roles](https://gitlab.com/cdetar/cfd-common-roles) to be in use as well.** Expects the following roles to be enabled:
  * `common`
  * `nginx`
  * `postgresql` (only with `k-django` when `django_database_postgresql` is `yes`)
  * `mysql` (only with `k-django` when `django_database_mysql` is `yes`)
  * `letsencrypt-nginx` (only when `django_proxy_ssl`, `flask_proxy_ssl`, `static_proxy_ssl`, or `monit_proxy_ssl` is `yes`)
  * `letsencrypt` (only when `django_proxy_ssl`, `flask_proxy_ssl`, `static_proxy_ssl`, or `monit_proxy_ssl` is `yes`)
  * `redis` (only with `k-django` when `django_celery_worker`, `django_celery_beat`, or `django_proxy_asgi` is `yes`)

Designed for and works well with [kjpc-django-template](https://gitlab.com/kjpc-tech/kjpc-django-template), [kjpc-flask-template](https://gitlab.com/kjpc-tech/kjpc-flask-template), and [kjpc-static-template](https://gitlab.com/kjpc-tech/kjpc-static-template).

---
## KJ PC | [kjpc.tech](https://kjpc.tech/) | [kyle@kjpc.tech](mailto:kyle@kjpc.tech)
---

## k-django
Role for a [Django](https://www.djangoproject.com/) website.
### Vars
  - `django_repo_prerequisites`: extra packages to install via apt
  - `django_repo_url`: Git repository url for Django project
  - `django_repo_branch`: repository branch to checkout (default `master`)
  - `django_repo_private_key`: private key with access to `django_repo_url`
  - `django_repo_dir`: destination path for Django project (default `"/home/{{ main_user_name }}/{{ django_project_name }}/"`)
  - `django_project_name`: name of django project
  - `django_project_host_name`: domain where Django website should be available (default `"{{ domain }}"` (from `common`))
  - `django_project_folder_name`: name of django project folder (contains manage.py) (default `"{{ django_project_name }}"`)
  - `django_project_dir`: Django project path (contains manage.py) (default `"{{ django_repo_dir }}{{ django_project_folder_name }}"`)
  - `django_project_root`: Django project root (contains wsgi.py and settings.py) (default `"{{ django_project_dir }}{{ django_project_folder_name }}"`)
  - `django_project_venv_dir`: virtual environment path (default `"{{ django_repo_dir }}venv/"`)
  - `django_project_logs_dir`: log file path (default `"{{ django_repo_dir }}logs/"`)
  - `django_project_dependencies_file`: python requirements file (default `"{{ django_repo_dir }}requirements/pinned.txt"`)
  - `django_settings_module`: value for DJANGO_SETTINGS_MODULE (default `"{{ django_project_folder_name }}.settings.production"`)
  - `django_settings_template`: j2 template for the Django env (or settings) file
  - `django_settings_destination`: destination path for Django env (or settings) file (default `"{{ django_project_dir }}.env"`)
  - `django_settings_static_url`: Django's media URL (default `"/static"`)
  - `django_settings_static_dir`: Django's collected static file directory (default `"{{ django_repo_dir }}/static_files"`)
  - `django_settings_media_url`: Django's media URL (default `"/media"`)
  - `django_settings_media_dir`: Django's media directory (default `"{{ django_repo_dir }}/media_files"`)
  - `django_database_name`: projects database name
  - `django_database_user`: projects database user
  - `django_database_password`: projects database password (when `django_local_backups` is `yes`, the `django_database_password` cannot have a colon (`:`) in it)
  - `django_database_postgresql`: when `yes`, uses PostgreSQL as the database (requires the `postgresql` role to be enabled) (default `no`)
  - `django_database_mysql`: when `yes`, uses MySQL as the database (requires the `mysql` role to be enabled) (default `no`)
  - `django_database_sqlite`: when `yes`, uses Sqlite3 as the database (default `no`)
  - `django_database_geodjango`: when `yes`, extra packages will be installed and the database will use a gis backend (default `no`)
  - `django_superuser_username`: when defined along with `django_superuser_email` and `django_superuser_password`, a superuser will be created (note, superuser won't be updated if variables change)
  - `django_superuser_email`: when defined along with `django_superuser_username` and `django_superuser_password`, a superuser will be created (note, superuser won't be updated if variables change)
  - `django_superuser_password`: when defined along with `django_superuser_username` and `django_superuser_email`, a superuser will be created (note, superuser won't be updated if variables change)
  - `django_test`: when `yes`, runs Django tests during deployment (default `no`)
  - `django_node`: when `yes`, installs NodeJS in the virtual environment (default `no`)
  - `django_node_version`: which NodeJS version to install (default `"latest"`)
  - `django_node_npm_version`: which NPM version to install (default `"latest"`)
  - `django_node_commands`: list of commands to run after NodeJS installation
  - `django_celery_app_name`: the Celery app name (default `"{{ django_project_folder_name }}"`)
  - `django_celery_worker`: when `yes`, adds a Celery worker service (default `no`)
  - `django_celery_worker_nodes`: the Celery worker nodes (default `"{{ django_project_folder_name }}1"`)
  - `django_celery_worker_log_file`: the `--logfile` flag for the celery workers (default `"{{ django_project_logs_dir }}celery_worker%N_log.txt"`)
  - `django_celery_worker_pid_file`: the `--pidfile` flag for the celery workers (default `"{{ django_repo_dir }}celery_worker%N.pid"`)
  - `django_celery_worker_concurrency`: the `--concurrency` flag for the celery worker (default `2`)
  - `django_celery_worker_max_memory_per_child`: the `--max-memory-per-child` flag for the celery worker
  - `django_celery_beat`: when `yes`, adds a Celery beat service (default `no`)
  - `django_celery_beat_log_file`: the `--logfile` flag for the celery beat service (default `"{{ django_project_logs_dir }}celery_beat_log.txt"`)
  - `django_celery_beat_pid_file`: the `--pidfile` flag for the celery beat service (default `"{{ django_repo_dir }}celery_beat.pid"`)
  - `django_proxy_wsgi`: when `yes`, serves the site with uwsgi (default `no`)
  - `django_proxy_wsgi_port`: port to serve the uwsgi instance (default `3030`)
  - `django_proxy_wsgi_module`: path to wsgi file from `django_project_dir` (ex `"project.wsgi"`) (default: `"{{ django_project_folder_name }}.wsgi"`)
  - `django_proxy_wsgi_processes`: the `--processes` flag for uwsgi (default `2`)
  - `django_proxy_asgi`: when `yes`, servers the site with daphne (default `no`)
  - `django_proxy_asgi_port`: port to serve the daphne instance (default `3040`)
  - `django_proxy_asgi_module`: path to asgi file from `django_project_dir` (ex `"project.asgi:application"`) (default: `"{{ django_project_folder_name }}.asgi:application"`)
  - `django_proxy_ssl`: when `yes`, uses https instead of http (requires the `letsencrypt` role to be enabled) (default `yes`)
  - `django_proxy_template`: path to template for static site nginx configuration (default `"django.nginx.proxy.j2"`)
  - `django_proxy_auth_basic_user`: restricts access to website with basic auth when user defined along with `django_proxy_auth_basic_pass`
  - `django_proxy_auth_basic_pass`: restricts access to website with basic auth when pass defined along with `django_proxy_auth_basic_user`
  - `django_proxy_robots_txt`: path to robots.txt file to add to the site
  - `django_local_backups`: when `yes`, the database will be dumped locally on a cron schedule (default `no`)
  - `django_local_backups_cron_minute`: crontab minute to run local backup script (default `"0"`)
  - `django_local_backups_cron_hour`: crontab hour to run local backup script (default `"10"`)

---

## k-flask
Role for a [Flask](http://flask.pocoo.org/) website.
### Vars
  - `flask_repo_prerequisites`: extra packages to install via apt
  - `flask_repo_url`: Git repository url for Flask project
  - `flask_repo_branch`: repository branch to checkout (default `master`)
  - `flask_repo_private_key`: private key with access to `flask_repo_url`
  - `flask_repo_dir`: destination path for Flask project (default `"/home/{{ main_user_name }}/{{ flask_project_name }}/"`)
  - `flask_project_name`: name of flask project
  - `flask_project_host_name`: domain where Flask website should be available (default `"{{ domain }}"` (from `common`))
  - `flask_project_folder_name`: name of flask project folder (contains app.py) (default `"{{ flask_project_name }}"`)
  - `flask_project_dir`: Flask project path (contains app.py) (default `"{{ flask_repo_dir }}{{ flask_project_folder_name }}"`)
  - `flask_project_venv_dir`: virtual environment path (default `"{{ flask_repo_dir }}venv/"`)
  - `flask_project_dependencies_file`: python requirements file (default `"{{ flask_repo_dir }}requirements/pinned.txt"`)
  - `flask_settings_template`: j2 template for the Flask settings file (default `"flask.settings.py.j2"` which is in `kjpc-ansible-roles/roles/k-flask/templates/`)
  - `flask_settings_destination`: destination path for Flask settings file (default `"{{ flask_project_dir }}settings/production.py"`)
  - `flask_settings_from_email`: from email for emails sent with Flask
  - `flask_settings_admins`: name, email for recipients of Flask error emails (list of `{name: 'Name', email: 'email@example.com'}`)
  - `flask_settings_static_url`: Flask's media URL (default `"/static"`)
  - `flask_settings_static_dir`: Flask's collected static file directory (default `"{{ flask_project_dir }}/static"`)
  - `flask_database_sqlite`: when `yes`, installs Sqlite3 for use with a database (default `no`)
  - `flask_database_sqlite_commands`: list of command to instantiate the project database
  - `flask_node`: when `yes`, installs NodeJS in the virtual environment (default `no`)
  - `flask_node_version`: which NodeJS version to install (default `"latest"`)
  - `flask_node_npm_version`: which NPM version to install (default `"latest"`)
  - `flask_node_commands`: list of commands to run after NodeJS installation
  - `flask_test`: when `yes`, runs Flask tests during deployment (default `no`)
  - `flask_test_command`: command to run the project tests (default `"cd {{ flask_project_dir }} && pytest"`)
  - `flask_proxy_wsgi_port`: port to serve the uwsgi instance (default `4030`)
  - `flask_proxy_wsgi_module`: path to wsgi file from `flask_project_dir` (ex `"project.wsgi"`) (default: `"{{ flask_project_folder_name }}.wsgi"`)
  - `flask_proxy_wsgi_processes`: the `--processes` flag for uwsgi (default `2`)
  - `flask_proxy_ssl`: when `yes`, uses https instead of http (requires the `letsencrypt` role to be enabled) (default `yes`)
  - `flask_proxy_template`: path to template for static site nginx configuration (default `"flask.nginx.proxy.j2"`)
  - `flask_proxy_auth_basic_user`: restricts access to website with basic auth when user defined along with `flask_proxy_auth_basic_pass`
  - `flask_proxy_auth_basic_pass`: restricts access to website with basic auth when pass defined along with `flask_proxy_auth_basic_user`
  - `flask_proxy_robots_txt`: path to robots.txt file to add to the site

---

## k-static
Role for a static website.
### Vars
  - `static_project_name`: name of static project
  - `static_project_host_name`: domain where static website should be available (default `"{{ domain }}"` (from `common`))
  - `static_project_local_dir`: path to directory containing static site files on local machine
  - `static_project_remote_dir`: path to directory that should contain static site on remote machine (default: `"/srv/{{ static_project_name }}"`)
  - `static_proxy_ssl`: when `yes`, uses https instead of http (requires the `letsencrypt` role to be enabled) (default `yes`)
  - `static_proxy_template`: path to template for static site nginx configuration (default `"static.nginx.proxy.j2"`)
  - `static_proxy_auth_basic_user`: restricts access to website with basic auth when user defined along with `static_proxy_auth_basic_pass`
  - `static_proxy_auth_basic_pass`: restricts access to website with basic auth when pass defined along with `static_proxy_auth_basic_user`
  - `static_proxy_robots_txt`: path to robots.txt file to add to the site

---

## k-monit
Role for a [Monit](https://mmonit.com/monit/) instance.
### Vars
  - `monit_objects`: list of objects for Monit to monitor (default `- { filename: "root-system", template: "monit.system.j2", cpu_usage: "80%", cpu_cycles: "10", memory_usage: "70%", swap_usage: "10%" }`)
    - **Available Object Options**
      - `filename`: unique name for monit object
      - `template`: template for monit object
      - *system* - `template`: `"monit.system.j2"`
        - `cpu_usage`: percentage (such as `"80%"`)
        - `cpu_cycles`: cycles (such as `"10"`)
        - `memory_usage`: percentage
        - `swap_usage`: percentage
      - *process* - `template`: `"monit.process.j2"`
        - `process`: process (such as `"sshd"`)
        - `pidfile`: path (such as `"/run/process.pid"`) (*optional*)
        - `matching`: regex (such as `"sshd"`) (*optional*)
        - `host`: host (such as `"localhost"`) (*optional*)
        - `port`: port (such as `"8080"`) (*optional*)
        - `protocol`: protocol (such as `"ssh"`, `"memcache"`, `"http"`, etc) (*optional*)
        - `cpu`: percentage (such as `"40%"`) (*optional*)
        - `cpu_cycles`: cycles (such as `"10"`) (*optional*)
      - *program* - `template`: `"monit.program.j2"`
        - `program`: program (such as `"/usr/local/bin/program.sh"`)
      - *filesystem* - `template`: `"monit.filesystem.j2"`
        - `path`: path (such as `"/"`)
        - `space_usage`: percentage (such as `"75%"`)
      - *file* - `template`: `"monit.file.j2"`
        - `path`: path (such as `"/var/run/reboot-required"`)
        - `alert_on_exist`: `yes` or `no`
      - *host* - `template`: `"monit.host.j2"`
        - `domain`: domain (such as `"example.com"`)
        - `frontpages`: list of routes (such as `["/"]`)
        - `ssl`: `yes` or `no`
        - `ssl_expires`: days (such as `"30"`) (*optional*)
        - `timeout`: seconds (such as `"10"`) (*optional*)
        - `retry`: attempts (such as `"2"`) (*optional*)
  - `monit_config_from_email`: from address for Monit email alerts
  - `monit_config_alert_emails`: list of to address for Monit email alerts
  - `monit_config_http_port`: port to serve Monit web interface on (default `2812`)
  - `monit_config_allow_credentials`: list of credentials for users who can view Monit web interface (in username:password format)
  - `monit_proxy_host_name`: domain where Monit web interface should be available
  - `monit_proxy_port`: port where Monit web interface should be available (uses 80/443 unless defined)
  - `monit_proxy_ssl`: when `yes`, uses https proxy instead of http (requires the `letsencrypt` role to be enabled) (default `yes`)

---

### Change Log
#### 2.5
  - Use `npm ci` instead of `npm install`

#### 2.4
  - Removed `memcached` and `libmemcached-dev` from default `k-django` prereqs
  - Changed `django_repo_dir` default to `"/home/{{ main_user_name }}/{{ django_project_name }}/"`
  - Changed `flask_repo_dir` default to `"/home/{{ main_user_name }}/{{ flask_project_name }}/"`
  - Prefer `django_project_name` over `django_project_host_name` for proxy naming
  - Prefer `flask_project_name` over `flask_project_host_name` for proxy naming
  - Prefer `static_project_name` over `static_project_host_name` for proxy naming
  - Added `django_database_mysql` option

#### 2.3
  - Added `django_settings_module`
  - Require `django_settings_template`
  - Changed `django_settings_destination` to default to `"{{ django_project_dir }}.env"`
  - Removed `django_settings_secret_key`
  - Removed `django_settings_allowed_hosts`
  - Removed `django_settings_from_email`
  - Removed `django_settings_admins`

#### 2.2
  - Replaced `django_settings_admin_name` and `django_settings_admin_email` with `django_settings_admins` in `k-django`
  - Replaced `flask_settings_admin_name` and `flask_settings_admin_email` with `flask_settings_admins` in `k-flask`
  - Added `flask_test` setting to `k-flask`
  - Fixed deprecation warnings
  - Update `k-django` celery to restart after code change
  - Update nginx default site to include SSL
  - Changed `django_project_dependencies_file` to default to `requirements/pinned.txt`
  - Changed `flask_project_dependencies_file` to default to `requirements/pinned.txt`
  - Use non-root user for creating virtual environments
  - Use non-root user for running services
  - Added `django_celery_worker_max_memory_per_child` setting to `k-django`
  - Added robots.txt support

#### 2.1
  - Added `virtualenv_python: python3` to `k-django` and `k-flask`
  - Added `django_project_logs_dir` setting to `k-django`
  - Added `django_celery_log_file` setting to `k-django`
  - Removed `django_celery_log_file` setting in `k-django`
  - Added `django_celery_worker_nodes` setting in `k-django`
  - Added `django_celery_worker_log_file` setting in `k-django`
  - Added `django_celery_worker_pid_file` setting in `k-django`
  - Added `django_celery_beat_log_file` setting in `k-django`
  - Added `django_celery_beat_pid_file` setting in `k-django`
  - Updated `k-django` celery.worker.service and celery.beat.service
  - Added `django_test` setting in `k-django`
  - Replaced `django_celery` with `django_celery_worker` and `django_celery_beat` in `k-django`
  - Added `django_celery_app_name` setting in `k-django`

#### 2.0
  - Added `k-flask` role
  - Changed `k-django` `django_proxy_template` to `"django.nginx.proxy.j2"`
  - Changed `k-static` `static_proxy_template` to `"static.nginx.proxy.j2"`
  - Created and used `shared-files` and `shared-templates` directories
  - Added support for Sqlite3 database with `k-flask`
  - Switched node commands to be full command
  - Added support for running tests with `k-flask`
  - Added settings to `k-flask`

#### 1.9
  - Added `django_project_folder_name` setting with default `"{{ django_project_name }}"`
  - Switch uWSGI to individual mode rather than emperor
  - Add more options to k-monit `monit.host.j2`
  - Added `django_repo_prerequisites` setting
  - Changed names of k-django celery services
  - Changed default value of `django_node_version` to `"latest"`
  - Changed default value of `django_node_npm_version` to `"latest"`
  - Fixed `django_node` so it updates when necessary
  - Added `django_celery_worker_concurrency` with default value `2`
  - Added `django_proxy_wsgi_processes` with default value `2`

#### 1.8
  - Renamed `django_host_name` to `django_project_host_name`
  - Renamed `django_venv_dir` to `django_project_venv_dir`
  - Renamed `django_dependencies_file` to `django_project_dependencies_file`
  - Renamed `django_secret_key` to `django_settings_secret_key`
  - Renamed `django_allowed_hosts` to `django_settings_allowed_hosts`
  - Added default value to `django_settings_allowed_hosts`
  - Renamed `django_from_email` to `django_settings_from_email`
  - Renamed `django_admin_name` to `django_settings_admin_name`
  - Renamed `django_admin_email` to `django_settings_admin_email`
  - Added `django_settings_static_url` with default as `/static`
  - Renamed `django_static_dir` to `django_settings_static_dir`
  - Added `django_settings_media_url` with default as `/media`
  - Renamed `django_media_dir` to `django_settings_media_dir`
  - Renamed `django_project_database_name` to `django_database_name`
  - Renamed `django_project_database_user` to `django_database_user`
  - Renamed `django_project_database_password` to `django_database_password`
  - Renamed `django_db_postgresql` to `django_database_postgresql`
  - Renamed `django_db_sqlite` to `django_database_sqlite`
  - Renamed `django_geodjango` to `django_database_geodjango`
  - Renamed `django_npm_version` to `django_node_npm_version`
  - Renamed `django_uwsgi_proxy` to `django_proxy_wsgi`
  - Renamed `django_uwsgi_port` to `django_proxy_wsgi_port`
  - Added default value `3030` to `django_proxy_wsgi_port`
  - Deleted `django_uwsgi_stats_port`
  - Renamed `django_wsgi_module` to `django_proxy_wsgi_module`
  - Renamed `django_daphne_proxy` to `django_proxy_asgi`
  - Renamed `django_daphne_port` to `django_proxy_asgi_port`
  - Added default value `3040` to `django_proxy_asgi_port`
  - Renamed `django_asgi_module` to `django_proxy_asgi_module`
  - Renamed `django_ssl` to `django_proxy_ssl`
  - Renamed `django_nginx_template` to `django_proxy_template`
  - Renamed `django_auth_basic_user` to `django_proxy_auth_basic_user`
  - Renamed `django_auth_basic_pass` to `django_proxy_auth_basic_pass`
  - Added `static_project_name`
  - Renamed `static_host_name` to `static_project_host_name`
  - Renamed `static_ssl` to `static_proxy_ssl`
  - Renamed `static_nginx_template` to `static_proxy_template`
  - Renamed `static_auth_basic_user` to `static_proxy_auth_basic_user`
  - Renamed `static_auth_basic_pass` to `static_proxy_auth_basic_pass`
  - Renamed `monit_http_port` to `monit_config_http_port`
  - Removed `monit_http_address`
  - Renamed `monit_ssl` to monit_proxy_ssl
  - Renamed `monit_from_email` to `monit_config_from_email`
  - Renamed `monit_alert_emails` to `monit_config_alert_emails`
  - Renamed `monit_allow_credentials` to `monit_config_allow_credentials`
  - Renamed `monit_proxy_host_port` to `monit_proxy_port`

#### 1.7
  - Added `django_settings_destination` setting
  - Added `django_superuser_username`, `django_superuser_email`, and `django_superuser_password` for Django `createsuperuser` command
  - Changed `monit_allow_user` to `monit_allow_credentials`
  - Remade local Django backups, doesn't require a management command anymore
  - Configured nginx to ignore hidden files for `k-static`

#### 1.6
  - Added `django_node` settings and support
  - Added `django_project_name` setting
  - Added `django_celery` setting

#### 1.5
  - Added `k-static` role
  - Added `monit_proxy_host_port` setting
  - Added `django_host_name` setting

#### 1.4
  - Changed `git_ssh_private_key` to `django_repo_private_key` and added note in README
  - Added `django_dependencies_file` setting to allow names other than requirements-pinned.txt.
  - Removed `django_extra_settings_file` setting, create custom settings template and use `django_settings_template` instead
  - Added `django_nginx_template` to allow for custom nginx configurations
  - Added `django_settings_template` to allow for custom Django settings configurations

#### 1.3
  - Removed `django_google_analytics_tracking` setting, use `django_extra_settings_file` instead
  - Updated remote file permissions
  - Added support for Sqlite3
  - Now requires either `django_db_postgresql` or `django_db_sqlite` to be `yes`
  - Added support for GeoDjango (with either PostGIS or SpatiaLite)
  - SSL is not required anymore although it is enabled by default

#### 1.2
  - Added support for asgi with Daphne (now requires either `django_uwsgi_proxy` or `django_daphne_proxy`)
  - Added Django ALLOWED_HOSTS setting with `django_allowed_hosts`
  - Added `GOOGLE_ANALYTICS` setting for Django with `django_google_analytics_tracking`

#### 1.1  
  - Switched k-monit var `monit_alert_email` to list `monit_alert_emails`  

#### 1.0  
  - Initial version  

### License
[MIT License](LICENSE)
